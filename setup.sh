#!/bin/bash
# ^ contains the path to the shell that the script will use, it's a bash script
#Provision Script for Ubuntu 18.04

#Usefil bash notes: setting a var for a directory
#SOURCEDIR = /home/user/Documents 
#$1 repreesnts the first argument entered after the command itself
#echo $1

#update before install
sudo apt-get update

#Install needed software 
sudo apt-get install bridge-utils ebtables libev-dev python tcl8.5 tk8.5 libtk-img libreadline-dev pkg-config help2man mgen -y

#Install tools for net emulation and routing
sudo apt-get install net-tools inetutils-traceroute traceroute -y

#for utility services
sudo apt-get install isc-dhcp-server apache2 libpcap-dev radvd at -y

#for security services
sudo apt-get install ipsec-tools racoon -y 
#installing racoon opens a gui that needs a response (enter)

# install openvpn
sudo apt-get install openvpn -y

#for ryu
sudo apt-get install gcc python-dev libffi-dev libssl-dev libxml2-dev libxslt1-dev zlib1g-dev python-pip -y

#install ryu using pip, can also use source code from github
pip install ryu

#setup CORE
git clone https://github.com/coreemu/core
cd core
./install.sh -p /usr/local/
#sudo sudo -v | grep PATH

# retrieve easyrsa3 for key/cert generation, see keys.sh for generating keys/certifcates
git clone https://github.com/OpenVPN/easy-rsa

#examples
mkdir examples
cd examples
git clone https://github.com/peppelinux/Common-Open-Research-Emulator-CORE-Tutorials

