#!/bin/bash
#from https://coreemu.github.io/core/services/security.html

# navigate into easyrsa3 repo subdirectory that contains built binary
cd easy-rsa/easyrsa3

# initalize pki
./easyrsa init-pki

# build ca
./easyrsa build-ca

# generate and sign server keypair(s)
SERVER_NAME=server1
./easyrsa get-req $SERVER_NAME nopass
./easyrsa sign-req server $SERVER_NAME

# generate and sign client keypair(s)
CLIENT_NAME=client1
./easyrsa get-req $CLIENT_NAME nopass
./easyrsa sign-req client $CLIENT_NAME

# DH generation
./easyrsa gen-dh

# create directory for keys for CORE to use
# NOTE: the default is set to a directory that requires using sudo, but can be
# anywhere and not require sudo at all
KEYDIR=/etc/core/keys
sudo mkdir $KEYDIR

# move keys to directory
sudo cp pki/ca.crt $KEYDIR
sudo cp pki/issued/*.crt $KEYDIR
sudo cp pki/private/*.key $KEYDIR
sudo cp pki/dh.pem $KEYDIR/dh1024.pem